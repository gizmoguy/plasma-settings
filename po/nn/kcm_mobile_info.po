# Translation of kcm_mobile_info to Norwegian Nynorsk
#
# Karl Ove Hufthammer <karl@huftis.org>, 2020.
msgid ""
msgstr ""
"Project-Id-Version: plasma-settings\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-07-07 00:47+0000\n"
"PO-Revision-Date: 2020-05-02 22:54+0200\n"
"Last-Translator: Karl Ove Hufthammer <karl@huftis.org>\n"
"Language-Team: Norwegian Nynorsk <l10n-no@lister.huftis.org>\n"
"Language: nn\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 20.04.0\n"
"X-Environment: kde\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Karl Ove Hufthammer"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "karl@huftis.org"

#: info.cpp:23
#, kde-format
msgid "Info"
msgstr "Info"

#: info.cpp:24
#, kde-format
msgid "Jonah Brüchert"
msgstr "Jonah Brüchert"

#: package/contents/ui/main.qml:16
#, kde-format
msgid "System Information"
msgstr "Systeminformasjon"

#: package/contents/ui/main.qml:42
#, kde-format
msgid "Operating System"
msgstr ""

#: package/contents/ui/main.qml:49
#, kde-format
msgid "Webpage"
msgstr ""

#: package/contents/ui/main.qml:70
#, kde-format
msgid "KDE Plasma Version"
msgstr ""

#: package/contents/ui/main.qml:77
#, kde-format
msgid "KDE Frameworks Version"
msgstr ""

#: package/contents/ui/main.qml:84
#, kde-format
msgid "Qt Version"
msgstr ""

#: package/contents/ui/main.qml:91
#, kde-format
msgid "Kernel Version"
msgstr ""

#: package/contents/ui/main.qml:98
#, kde-format
msgid "OS Type"
msgstr ""

#: package/contents/ui/main.qml:99
#, kde-format
msgctxt "@label %1 is the CPU bit width (e.g. 32 or 64)"
msgid "%1-bit"
msgstr "%1-bit"

#: package/contents/ui/main.qml:116
#, kde-format
msgid "Processor"
msgid_plural "Processors"
msgstr[0] ""
msgstr[1] ""

#: package/contents/ui/main.qml:123
#, kde-format
msgid "Memory"
msgstr ""

#: package/contents/ui/main.qml:127
#, kde-format
msgctxt "@label %1 is the formatted amount of system memory (e.g. 7,7 GiB)"
msgid "%1 of RAM"
msgstr "%1 RAM"

#: package/contents/ui/main.qml:129
#, kde-format
msgctxt "Unknown amount of RAM"
msgid "Unknown"
msgstr "Ukjend"

#: package/contents/ui/main.qml:143
#, kde-format
msgid "Copy to clipboard"
msgstr "Kopier til utklippstavla"
